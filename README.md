# Inventory_App

Backend Developer Test for CB_Plus

Minimal inventory app for a small store, with a sync mechanism, backed by a PostgreSQL DB
Simultaneous connexions
Sync by refresh


1) Setup a virtualenv python with : python3 -m venv <virtualenv name> 
2) Activate it with source <virtualenv name>/bin/activate
3) pip install -r requirement.txt
4) go to inventory_app/inventory_app/inventory_app
5) setup the postgres settings in the inventory_app/inventory_app/inventory_app/settings.py (line 92)
6) go to inventory_app/inventory_app
7) launch with ./run.sh




