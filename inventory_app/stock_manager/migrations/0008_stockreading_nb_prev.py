# Generated by Django 3.0.8 on 2020-07-26 19:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('stock_manager', '0007_auto_20200726_1818'),
    ]

    operations = [
        migrations.AddField(
            model_name='stockreading',
            name='nb_prev',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]
