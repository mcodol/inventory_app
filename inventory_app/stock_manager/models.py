from django.db import models
from django.urls import reverse
from django.core.validators import MinLengthValidator

# Create your models here.

class TimestamptedModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True
        

class Shop(TimestamptedModel):
    name    = models.CharField(max_length=255, unique=True)
    owner   = models.CharField(max_length=255, default="BigBoss")
    address = models.TextField()

    class Meta:
        ordering = ['name']

    def get_absolute_url(self):
        return reverse('stock_manager:show', kwargs={'id': self.id})


class Product(TimestamptedModel):
    name      = models.CharField(max_length=255, unique=True)
    reference = models.CharField(max_length=13, validators=[MinLengthValidator(13)], unique=True)
    shop   = models.ForeignKey(Shop, on_delete=models.CASCADE)
    expiry_date = models.DateTimeField()

    class Meta:
        ordering = ['expiry_date']

#    def get_absolute_url(self):
#        return reverse('stock_manager:show', kwargs={'id': self.id})


class StockReading(TimestamptedModel):
    
    shop     = models.ForeignKey(Shop, on_delete=models.CASCADE)
    product  = models.ForeignKey(Product, on_delete=models.CASCADE)
    nb_prev  = models.IntegerField()

    def __str__(self):
        return "ref={}, DLV = {}".format(self.product.reference, self.product.expiry_date)
