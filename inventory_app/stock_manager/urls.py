from django.urls import path, re_path

from . import views

app_name='stock_manager'

urlpatterns = [
    path('', views.index, name='index'),
    
    re_path(r'^shop/(?P<id>[0-9]+)$', views.show, name='show'),
    path('shop/add/', views.ShopCreate.as_view(), name='shop-add'),
    path('shop/<int:pk>/', views.ShopUpdate.as_view(), name='shop-update'),
    path('shop/<int:pk>/delete/',  views.ShopDelete.as_view(), name='shop-delete'),

    re_path(r'^StockReading/(?P<id>[0-9]+)$', views.sreading, name='sreading'),


]


"""
#    path('product/add/', views.ProductCreate.as_view(), name='product-add'),
    re_path(r'^product/(?P<id>[0-9]+)$/add/', views.ProductCreate.as_view(), name='product-add'),
    path('product/<int:pk>/', views.ProductUpdate.as_view(), name='product-update'),
    path('product/<int:pk>/delete/',  views.ProductDelete.as_view(), name='product-delete'),
"""
