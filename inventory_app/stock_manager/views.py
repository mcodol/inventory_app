from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.shortcuts import render, get_object_or_404
from django.views.generic import CreateView, UpdateView, DeleteView

from .models import Shop, Product, StockReading

import datetime

# Create your views here.


def index(request):
    shops = Shop.objects.all()

    return render(request, 'stock_manager/index.html', {'shops': shops})


def show(request, id):
    shop = get_object_or_404(Shop, pk=id)
    products = Product.objects.filter(shop__pk=id)
    stockreadings = []

    if len(products) > 0:
        for product in products:

            nb_prev = len(StockReading.objects.filter(product__pk=product.id)) + 1
            stockreadings.append(StockReading(shop = shop, product = product, nb_prev = nb_prev))
            stockreadings[-1].save()
    return render(request, 'stock_manager/shop.html', {'shop': shop, 'products': products, 'stockreadings':stockreadings})

def sreading(request, id):
    stockreadings = StockReading.objects.filter(product__pk=id)
    if len(stockreadings)>0:
        sname = stockreadings[0].shop.name
        pname = stockreadings[0].product.name
        rid   = stockreadings[0].product.reference
        dlv   = stockreadings[0].product.expiry_date

        return render(request, 'stock_manager/sreading.html', {'stockreadings': stockreadings, 'sname':sname, 'pname': pname, 'rid': rid, 'dlv': dlv})


class ShopCreate(CreateView):
    model = Shop
    fields = ['name', 'owner', 'address']

class ShopUpdate(UpdateView):
    model = Shop
    fields = ['name', 'owner', 'address']

class ShopDelete(DeleteView):
    model = Shop
    success_url = reverse_lazy('stock_manager:index')

"""
class ProductCreate(CreateView):
    model = Product
    fields = ['name', 'reference', 'expiry_date']

    def form_valid(self, form):
        form.instance.shop_id = self.request.shop.id 
        return super().form_valid(form)
    



class ProductUpdate(UpdateView):
    model = Product
    fields = ['name', 'reference', 'expiry_date']


class ProductDelete(DeleteView):
    model = Product
    success_url = reverse_lazy('stock_manager:index')
"""
